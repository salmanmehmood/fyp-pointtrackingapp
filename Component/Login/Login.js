import React from 'react';
import {Text,View,KeyboardAvoidingView,StyleSheet,ImageBackground ,TouchableOpacity, Alert, AsyncStorage, ActivityIndicator, ScrollView} from 'react-native';
import {Button,Item,Input,Icon } from 'native-base';
import {Link} from 'react-router-native';
import { connect } from 'react-redux';
import backgroundImage1 from '../../Images/background1.png';
import Middlewarefunc from '../../Component/store/middleware/Middleware';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './LoginStyle';
import Spinner from '../Shared/Spinner'

function mapStateToProp(state){
 return{
    user: state.AUTHENTICATION
 }   
}
function mapDispatchToProps(disptach){
    return{
        login: function(state,props){
            return disptach(Middlewarefunc.nodeLogin(state,props))
        }
    }
}

class Login extends React.Component{
    constructor(){
        super();
        this.state={
            email: '',
            password: '',
            isLoading: false,
            progress: false
        }
    }
    componentWillReceiveProps(nextProps){
        this.setState({isLoading: false})
        const {login} = this.props.screenProps;
        nextProps.user.status !== undefined ? login(nextProps.user.status, nextProps.user._id):null
    }

    componentDidMount(){
        this.getData()
    }

    onChange(name, val) {
        this.setState({ [name] : val });
    }

    getData = async () => {
        try {
          const {login} = this.props.screenProps;
          const value = await AsyncStorage.getItem('user').then(val => {
            if (val !== null) {
              let parsed = JSON.parse(val);
              this.setState({
                  progress: true,
                  email: '',
                  password: ''
              })                                                 
              login(parsed.status, parsed._id)
            }              
            else { 
                this.setState({
                    progress: true
                })
            }
          })
        }
        catch (e) {}
    }

    login=() => {
        const {email, password} = this.state;
        const {login} = this.props.screenProps;
        if(!(email && password)){
            Alert.alert('Message', 'Fill All Fields')
        }
        else{ 
            this.setState({isLoading: true})
            this.props.login(this.state,this.props)
        }
    }
    loginUi = ()=> {               
        return(
            <View style={styles.container}>
                <ImageBackground source ={backgroundImage1} style={styles.bgstyle}>
                    <KeyboardAwareScrollView contentContainerStyle={{ justifyContent: 'center', alignItems: 'center',marginVertical: 40 }}>
                        <View style={styles.setLogin}>
                            <View style={styles.login2}>
                                <TouchableOpacity onPress={()=>this.props.navigation.navigate('Signup')}>
                                    <Text style={styles.loginhead}> SIGNUP </Text>
                                </TouchableOpacity>   
                                <View>
                                    <View style={styles.container2}>
                                        <View style={styles.login}>
                                            <Text style={styles.head}> LOGIN </Text>
                                            <Item style={[styles.textField,styles.input,styles.username]}>
                                                <Icon active name='ios-mail' style={styles.iconsenvelope} />
                                                <Input placeholder = 'Email' style={styles.underline} placeholderTextColor="#C0C0C0"
                                                onChangeText={this.onChange.bind(this, 'email')}/>
                                            </Item>
                                            <Item style={[styles.textField,styles.input]}>
                                                <Icon active name='lock' style={styles.iconslock} />
                                                <Input placeholder = 'Password' secureTextEntry={true} style={styles.underline}
                                                onChangeText={this.onChange.bind(this, 'password')} placeholderTextColor="#C0C0C0"/>
                                            </Item>
                                            <View>
                                                <TouchableOpacity style={[styles.signbtn,styles.btnLogin]} onPress={this.login}>
                                                    <Text style={styles.signupText}>SUBMIT</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View> 
                                    </View>
                                </View>
                            </View>
                        </View>
                        <Spinner color="#fff" isLoading={this.state.isLoading} isOverlaySpinner={true}></Spinner>
                    </KeyboardAwareScrollView>
                </ImageBackground>
            </View>
        )
    }
    render(){
        return(
               this.state.progress ? this.loginUi() : <View style={{flex: 1, justifyContent: 'center'}}><ActivityIndicator size={55}/></View>
        )
    };
}
export default connect(mapStateToProp, mapDispatchToProps)(Login) ;
