import React from 'react';
import { Text, View, ImageBackground, TouchableOpacity, Alert, AsyncStorage, ActivityIndicator, BackHandler, DeviceEventEmitter } from 'react-native';
import { Button,Item,Input,ListItem,Radio,Right,Container,Content ,Icon } from 'native-base';
import {Link} from 'react-router-native';
import backgroundImage from '../../Images/background.png';
import backgroundImage1 from '../../Images/background1.png';
import axios from 'axios';
import SignUpStyle from './SignupStyles';
import Middlewarefunc from '../../Component/store/middleware/Middleware';
import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";

function mapStateToProp(state){
 return{}   
}

function mapDispatchToProps(disptach){
    return{
        signUp: function(state,props){
            return disptach(Middlewarefunc.nodeSignUp(state, props))
        }
    }
}

class Signup extends React.Component{
    constructor(){
        super();
        this.state={
            gender: 'male',
            page1: true , 
            status: 'student',
            firstName: '',
            lastName: '',
            email: '',
            confirmPsw: '',
            password: '',
            latitude: null,
            longitude: null,
            progress: false
        }
    }

    onChange(name, val) {
        this.setState({ [name] : val });
    }
    
    componentDidMount(){
        this.enableUserLocation();
        this.getCurrentLocation()
    }

    getCurrentLocation(){
        this.watchID = navigator.geolocation.getCurrentPosition((position)=>{
            this.setState({
                latitude : position.coords.latitude,
                longitude : position.coords.longitude,
            }),
            (err)=>console.log(err),
            { enableHighAccuracy: true, timeout: 200000, maximumAge: 1000 }
        })
    }

    enableUserLocation = () =>{
        LocationServicesDialogBox.checkLocationServicesIsEnabled({
            message: "<h2>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/><a href='#'>Learn more</a>",
            ok: "YES",
            cancel: "NO",
            enableHighAccuracy: true,
            showDialog: true, 
            openLocationServices: true, 
            preventOutSideTouch: false, 
            preventBackClick: false, 
            providerListener: false 
        }).then(function(success) {
            console.log(success); 
        }).catch((error) => {
            console.log(error.message);
        });
    }
    
    signUpAuthentication = () => {
        const { firstName, lastName, email, password, confirmPsw, status, gender} = this.state;
        if(firstName === '' && lastName === '' && email === '' && password === '' && confirmPsw === ''){
            Alert.alert('Error', 'Fill All Fields')                                    
        }  
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(reg.test(email) === true){
            if(password.length < 6) {
                Alert.alert('Error', 'Password should be atleast 6 characters')
            }
            else if(password === confirmPsw){
                this.setState({ progress: true })
                this.props.signUp(this.state, this.props)   
            }
            else{
                Alert.alert('Error', 'Password not matched')
            }
        }
        else{                                 
            Alert.alert('Error', 'invalid email')               
        }    
    }

    signup1 = () =>{
        return(
            <View style= {SignUpStyle.container}>
                <ImageBackground source ={backgroundImage} style={SignUpStyle.bgstyle}>
                    <KeyboardAwareScrollView contentContainerStyle={{ justifyContent: 'center', alignItems: 'center',marginVertical: 40 }}>
                        <View style={SignUpStyle.setLogin}>
                            <View style={SignUpStyle.container1}>
                                <View style={SignUpStyle.login2}>
                                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Login')}>
                                        <Text style={SignUpStyle.loginhead}> LOGIN </Text>
                                    </TouchableOpacity>
                                <View style={SignUpStyle.login}>
                                    <Text style={SignUpStyle.head}> SIGN UP </Text>
                                    <Item style={[SignUpStyle.textField,SignUpStyle.input]}>
                                        <Icon active name='ios-person-add' style={SignUpStyle.icons}  />
                                        <Input placeholder = 'Firstname' value={this.state.firstName} style={SignUpStyle.underline} placeholderTextColor="#C0C0C0"
                                        onChangeText={this.onChange.bind(this, 'firstName')}
                                        />
                                    </Item>
                                    <Item style={SignUpStyle.textField}>
                                        <Icon active name='ios-person-add' style={SignUpStyle.icons} />
                                        <Input placeholder = 'Lastname' value={this.state.lastName} style={SignUpStyle.underline} placeholderTextColor="#C0C0C0"
                                        onChangeText={this.onChange.bind(this, 'lastName')}
                                        />
                                    </Item>
                                <View style = {SignUpStyle.radio}>
                                    <View style={SignUpStyle.gender}>
                                        <Icon active name='transgender' style={SignUpStyle.iconsgender}></Icon>
                                        <Text style= {[SignUpStyle.radioText,SignUpStyle.gendertxt ]}>Gender</Text>
                                    </View>
                                    <View style={SignUpStyle.List}>
                                        <View style = {SignUpStyle.radioList}>
                                            <ListItem style = {SignUpStyle.border}>
                                                <Radio onPress={()=> this.setState({gender : 'male'})} selected={this.state.gender === "male"}/>
                                                <Text style={SignUpStyle.txt}>  Male</Text>
                                            </ListItem>
                                        </View>
                                        <View style={{marginTop : -15,}}>
                                            <ListItem style = {SignUpStyle.border}>
                                                <Radio onPress={()=> this.setState({gender : 'female'})} selected={this.state.gender === "female"}/>
                                                <Text style={SignUpStyle.txt}>  Female</Text>
                                            </ListItem>
                                        </View>
                                    </View>
                                </View>
                                <View style={SignUpStyle.nxtpage}>
                                    <TouchableOpacity style={{elevation:6}}>
                                        <Text style={SignUpStyle.btn1}>1</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{elevation:6}} onPress={()=> this.setState({page1 : false})}>
                                        <Text style={SignUpStyle.btn2}>2</Text>
                                    </TouchableOpacity>
                                </View>
                                </View> 
                                </View>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </ImageBackground>      
            </View>
        )
    }
    signup2 = () =>{
        return(
            <View style={SignUpStyle.container}>
            <ImageBackground source ={backgroundImage} style={SignUpStyle.bgstyle}>
            <KeyboardAwareScrollView contentContainerStyle={{ justifyContent: 'center', alignItems: 'center',marginVertical: 40 }}>
            <View style={SignUpStyle.setLogin}>
                <View style={SignUpStyle.login2}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Login')}>
                        <Text style={SignUpStyle.loginhead}> LOGIN </Text>
                    </TouchableOpacity>
                <View>
                    <View style={SignUpStyle.container2}>
                        <View style={SignUpStyle.login}>
                            <Text style={SignUpStyle.head}> SIGNUP </Text>
                            <Item style={[SignUpStyle.textField,SignUpStyle.marginemail]}>
                                <Icon active name='ios-mail' style={SignUpStyle.iconsenvelope} />
                                <Input placeholder = 'Email' value={this.state.email} style={SignUpStyle.underline} placeholderTextColor="#C0C0C0"
                                onChangeText={this.onChange.bind(this, 'email')}/>
                            </Item>
                            <Item style={[SignUpStyle.textField,SignUpStyle.marginpass]}>
                                <Icon active name='lock' style={SignUpStyle.iconslock} />
                                <Input placeholder = 'Password' value={this.state.password} secureTextEntry={true} style={SignUpStyle.underline} placeholderTextColor="#C0C0C0"
                                onChangeText={this.onChange.bind(this, 'password')}/>
                            </Item>
                            <Item style={[SignUpStyle.textField,SignUpStyle.marginpass]}>
                                <Icon active name='lock' style={SignUpStyle.iconslock} />
                                <Input placeholder = 'ConfirmPassword' value={this.state.confirmPsw} secureTextEntry={true} style={SignUpStyle.underline} placeholderTextColor="#C0C0C0"
                                onChangeText={this.onChange.bind(this, 'confirmPsw')}
                                />
                            </Item>
                            <View style = {SignUpStyle.radio}>
                                <View style={[SignUpStyle.gender,SignUpStyle.marginAsa2]}>
                                    <Icon active name='ios-checkmark-outline' style={SignUpStyle.iconscheck}></Icon>
                                    <Text style= {[SignUpStyle.radioText,SignUpStyle.gendertxt ]}>As a :</Text>
                                </View>
                                <View style={[SignUpStyle.List,SignUpStyle.marginAsa]}>
                                    <View style = {SignUpStyle.radioList}>
                                        <ListItem style = {SignUpStyle.border}>
                                            <Radio onPress={()=> this.setState({status : 'student'})} selected={this.state.status === "student"}/>
                                            <Text style={SignUpStyle.txt}>  Student  </Text>
                                            <Radio onPress={()=> this.setState({status : 'parent'})} selected={this.state.status === "parent"}/>
                                            <Text style={SignUpStyle.txt}>  Parent</Text>
                                        </ListItem>
                                    </View>
                                </View>
                            </View>
                            <View>
                                <TouchableOpacity style = {SignUpStyle.signbtn} onPress={this.signUpAuthentication}>
                                    <Text style={SignUpStyle.signupText}>SUBMIT</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[SignUpStyle.nxtpage,SignUpStyle.footer]}>
                                <TouchableOpacity onPress={()=> this.setState({page1 : true})}>
                                    <Text style={SignUpStyle.btn3} >1</Text>
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Text style={SignUpStyle.btn4}>2</Text>
                                </TouchableOpacity>
                            </View>
                        </View> 
                    </View>
                </View>
                </View>
            </View>
            </KeyboardAwareScrollView>
            </ImageBackground>
            </View>
        )
    }
    render(){
        const {page1, progress} = this.state;
        return(
        progress? (this.state.progress ? <View style={{flex: 1, justifyContent: 'center'}}><ActivityIndicator size={55}/></View>: <View/>)
        : (page1 ? this.signup1(): this.signup2()) 
        )
    }
}
export default connect(mapStateToProp, mapDispatchToProps)(Signup)



