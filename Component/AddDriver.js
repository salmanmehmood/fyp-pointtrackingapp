import React from 'react';
import { Text, View, StyleSheet, ImageBackground, ToastAndroid } from 'react-native';
import { Button, Item, Input } from 'native-base';
import {Link} from 'react-router-native';
import backgroundImage from '../Images/background.png';
import styles from './css';
import { TextField } from 'react-native-material-textfield';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Spinner from './Shared/Spinner';
import Icon from 'react-native-vector-icons/FontAwesome';

//redux connect
import { connect } from 'react-redux';
import Middlewarefunc from '../Component/store/middleware/Middleware';

class AddDriver extends React.Component{
    constructor(props){
        super(props);
        this.onFocus = this.onFocus.bind(this);
        this.state={
            firstName: '',
            lastName: '',
            isLoading: false,
            email: '',
            password: '',
            point_location: '',
            status: 'driver'
        }
    }

    onChange(name, val){
        this.setState({
            [name]: val
        })
    }
    onFocus() {
        let { errors = {} } = this.state;
        for (let name in errors) {
            let ref = this[name];
            if (ref && ref.isFocused()) {
                delete errors[name];
            }
        }
        this.setState({ errors });
    }

    addDriver = () => {
        let { firstName, lastName, email, password, point_location } = this.state;
        let errors = {};
        ['first_name', 'last_name', 'email', 'password', 'point_location' ]
            .forEach((name) => {
                let value = this[name].value();
                if(!value) {
                    errors[name] = `${[name]} is a required field`;
                }
            })
            this.setState({errors},()=>{
                if(Object.keys(errors).length === 0){
                    this.props.addDriver12(this.state, this.props)
                    this.setState({
                        firstName: '',
                        lastName: '',
                        email: '',
                        password: '',
                        point_location: '',
                    })
                }
            })
    }   
    render(){
        let { errors = {}, firstName, lastName, email, password, point_location } = this.state;
        return(
            <KeyboardAwareScrollView>
                <View>
                    <View >
                        <View >
                            <View style={{marginHorizontal: 20}}>
                                <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
                                    <Text style={styles.head}> ADD DRIVER </Text>
                                </View>
                                <View >
                                    <TextField onSubmitEditing={() => this.last_name.focus()}
                                        ref={(ref) => { this.first_name = ref }}
                                        value={firstName}
                                        onChangeText={this.onChange.bind(this, 'firstName')}
                                        tintColor={'#0b6623'}
                                        onFocus={this.onFocus}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        label='First Name' 
                                        error={errors.first_name}/>
                                </View>
                                <View >
                                    <TextField onSubmitEditing={() => this.email.focus()}
                                        ref={(ref) => { this.last_name = ref }}
                                        onChangeText={this.onChange.bind(this, 'lastName')}
                                        tintColor={'#0b6623'}
                                        value={lastName}
                                        onFocus={this.onFocus}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        label='Last Name' 
                                        error={errors.last_name}/>
                                </View>
                                <View >
                                    <TextField onSubmitEditing={() => this.password.focus()}
                                        ref={(ref) => { this.email = ref }}
                                        onChangeText={this.onChange.bind(this, 'email')}
                                        tintColor={'#0b6623'}
                                        value={email}
                                        onFocus={this.onFocus}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        label='Email' 
                                        error={errors.email}/>
                                </View>
                                <View >
                                    <TextField onSubmitEditing={() => this.point_location.focus()}
                                        ref={(ref) => { this.password = ref }}
                                        onChangeText={this.onChange.bind(this, 'password')}
                                        tintColor={'#0b6623'}
                                        secureTextEntry={true}
                                        value={password}
                                        onFocus={this.onFocus}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        label='Password' 
                                        error={errors.password}/>
                                </View>
                                <View >
                                    <TextField
                                        ref={(ref) => { this.point_location = ref }}
                                        onChangeText={this.onChange.bind(this, 'point_location')}
                                        tintColor={'#0b6623'}
                                        value={point_location}
                                        onFocus={this.onFocus}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        label='Point Location'
                                        error={errors.point_location} />
                                </View>
                                <Button block style={styles.button} onPress={this.addDriver}>
                                    <Text style = {styles.btnText}>Add</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                <Spinner color="#fff" isLoading={this.state.isLoading} isOverlaySpinner={true}></Spinner>
                </View>
            </KeyboardAwareScrollView>
        )
    }
}

function mapStateToProps(state){
    return{}
}
function mapDispatchToProps(dispatch){
    return{
        addDriver12: function(state, props){
            return dispatch(Middlewarefunc.addDriver(state, props))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddDriver) ;