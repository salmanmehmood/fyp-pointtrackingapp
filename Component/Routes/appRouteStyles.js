import { StyleSheet } from 'react-native';

export default CustomRouteStyle = StyleSheet.create({
    homeHeaderTitleStyle: {
        color: 'white', 
        marginHorizontal: '22%',
    },
    headerBackground: {
        backgroundColor: 'green' ,
    },
    menuIcon: {
        paddingLeft: 15
    },
    headerTitleView: {
        justifyContent: 'center', 
        flex: 1, 
        alignItems: 'center'
    },
    headerTitleText: {
        fontFamily: 'Raleway-Regular', 
        color:'white', 
        fontSize: 20
    }
})