import React from 'react';
import {Alert, ActivityIndicator, Platform, StyleSheet, Text, View, TouchableOpacity, AsyncStorage, TextInput, Dimensions, ToastAndroid} from 'react-native';
import {Container , Content, Header, Body, Left, Title, Card, CardItem, Input} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import DoneIcon from 'react-native-vector-icons/MaterialIcons';
import styles from './css';

const { width, height } = Dimensions.get('window');

export default class SettingScreen extends React.Component{
    constructor(){
        super();
        this.state = {
            firstName: '',
            lastName: '',
            updateFirstName: '',
            updateLastName: '',
            password: '',
            newPassword: '',
            confirmPassword: '',
            isConfirm: false,
            profile: []
        }
    }
    componentDidMount(){
        this.getData();
    }

    getData = () =>{
        var self = this;
        fetch(`https://rocky-stream-13314.herokuapp.com/getData/${this.props.screenProps._id}`,{
            method: 'GET',
        })
        .then(res => {
            res.json().then(function(data){
                let {record} = data;
                self.setState({
                    profile: data,
                    firstName: record.firstName,
                    lastName: record.lastName,
                })
            })
        })
    }

    onChange = (name, val) => {
        this.setState({ [name]: val })
    }

    updateProfile = (name) =>{
        let { firstName, lastName } = this.state;
        if(!(firstName  && lastName )){
            ToastAndroid.showWithGravityAndOffset(`Field Must Be Filled`, ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
        }
        else{
            var self = this;
                fetch(`https://rocky-stream-13314.herokuapp.com/updateProfile/${this.props.screenProps._id}`,{
                    method: 'POST',                
                    headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                    body: JSON.stringify({
                        [name]: this.state[name]
                    })
                }).then((res)=>{
                    res.json().then(function(data){
                        if(data.key){
                            self.setState({isConfirm: true})
                        }
                        ToastAndroid.showWithGravityAndOffset(`${data.message}`, ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
                    }) 
                })
            }
        }
    
    updatePassword = () =>{
        var self = this;
        const { newPassword, confirmPassword } = this.state;
        if(!(newPassword && confirmPassword)){
            ToastAndroid.showWithGravityAndOffset('Field Must Be Filled', ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
        }
        else{
            const { _id } = this.props.screenProps;
            if(newPassword === confirmPassword){
                fetch(`https://rocky-stream-13314.herokuapp.com/updatePassword/${_id}`,{
                    method: 'POST',                
                    headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                    body: JSON.stringify({
                        password: this.state.newPassword
                    })
                }).then((res)=>{
                    res.json().then(function(data){
                        if(data.key){
                            self.setState({
                                isConfirm: false,
                                password: '',
                            })
                        }
                        ToastAndroid.showWithGravityAndOffset(`${data.message}`, ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
                    }) 
                })
            }
            else{
                ToastAndroid.showWithGravityAndOffset('Password Mismatch', ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50)
            }
        }
    }

    render(){
        let { firstName, lastName, password, newPassword, confirmPassword, isConfirm, profile } = this.state
        return(
            profile.length !== 0 ?
            <Container>
            <Content padder>
                <Card style={{flex: 1}}>
                    <CardItem>
                        <Body>
                            <View style={{paddingBottom: 25, justifyContent: 'center', alignItems: 'center'}}>
                                <Text style={{fontSize: 20, fontFamily: 'Raleway-Bold'}}>Profile</Text>
                            </View>
                            <View style={{paddingHorizontal: 15}}>
                                <View style={stylez.container2}>
                                        <View style={stylez.underline}>
                                            <Icon name='user-circle' size={20} style={stylez.image} color="green"/> 
                                            <Text style={stylez.position1}> First Name </Text>
                                        </View>
                                    <View style={stylez.button1}>
                                        <TextInput underlineColorAndroid='transparent' onChangeText={this.onChange.bind(this, 'firstName')} value={firstName} ref={(ref) => { this.firstName = ref }} style={stylez.textfield}/>
                                        <Icon name='edit' size={20} style={{marginRight:5}}  onPress={()=> this.firstName.focus()}   color="green"/> 
                                        <DoneIcon name='cloud-done' onPress={()=>this.updateProfile('firstName')} size={25} color="green"/>
                                    </View>
                                </View>
                                <View style={stylez.container2}>
                                        <View style={stylez.underline}>
                                            <Icon name='user-circle' size={20} style={stylez.image} color="green"/> 
                                            <Text style={stylez.position1}> Last Name </Text>
                                        </View>
                                    <View style={stylez.button1}>
                                        <TextInput underlineColorAndroid='transparent' onChangeText={this.onChange.bind(this, 'lastName')}  value={lastName} ref={(ref) => { this.lastName = ref }} style={stylez.textfield}/>
                                        <Icon name='edit' size={20} style={{marginRight:5}} onPress={()=> this.lastName.focus()} color="green" /> 
                                        <DoneIcon name='cloud-done' onPress={()=>this.updateProfile('lastName')} size={25} color="green"/>
                                    </View>
                                </View>
                                <View style={stylez.container2}>
                                    <View style={stylez.underline}>
                                        <Icon name='lock' size={20} style={{marginRight:5}} color="green"/>
                                        <Text style={stylez.position1}> Old Password </Text>
                                    </View>
                                    <View style={stylez.button1}>
                                    <TextInput underlineColorAndroid='transparent' onChangeText={this.onChange.bind(this, 'password')} value={password} secureTextEntry={true} ref={(ref) => { this.password = ref }} style={stylez.textfield}/>
                                        <Icon name='edit' size={20} style={{marginRight:5}}  onPress={()=> this.password.focus()}   color="green"/> 
                                        <DoneIcon name='cloud-done' size={25} color="green" onPress={()=>this.updateProfile('password')}/> 
                                    
                                    </View>
                                </View>
                                {
                                    isConfirm ? 
                                    <View>
                                        <View style={stylez.container2}>
                                            <View style={stylez.underline}>
                                                <Icon name='lock' size={20} style={{marginRight:5}} color="green"/>
                                                <Text style={stylez.position1}> New Password </Text>
                                            </View>
                                            <View style={stylez.button1}>
                                            <TextInput underlineColorAndroid='transparent' onChangeText={this.onChange.bind(this, 'newPassword')} value={newPassword} secureTextEntry={true} ref={(ref) => { this.newPassword = ref }} style={stylez.textfield}/>
                                                <Icon name='edit' size={20} style={{marginRight:5}}  onPress={()=> this.newPassword.focus()}   color="green"/> 
                                                <DoneIcon name='cloud-done' size={25} color="green"/> 
                                            </View>
                                        </View>
                                        <View style={stylez.container2}>
                                            <View style={stylez.underline}>
                                                <Icon name='lock' size={20} style={{marginRight:5}} color="green"/>
                                                <Text style={stylez.position1}> Confirm Password </Text>
                                            </View>
                                            <View style={stylez.button1}>
                                            <TextInput underlineColorAndroid='transparent' onChangeText={this.onChange.bind(this, 'confirmPassword')} value={confirmPassword} secureTextEntry={true} ref={(ref) => { this.confirmPassword = ref }} style={stylez.textfield}/>
                                                <Icon name='edit' size={20} style={{marginRight:5}}  onPress={()=> this.confirmPassword.focus()}   color="green"/> 
                                                <DoneIcon name='cloud-done' onPress={this.updatePassword} size={25} color="green"/> 
                                            
                                            </View>
                                        </View>
                                    </View>: <View/>
                                }
                            </View>
                    </Body>
                </CardItem>
            </Card>
      </Content>
    </Container>:<View style={stylez.Indicator}><ActivityIndicator size={50}/></View> 
        )
    }
}

const stylez = StyleSheet.create({
    container: {
      flex: 3,
      backgroundColor: '#F5FCFF',    
    },
    mainView : {
      borderBottomWidth : 2,
      borderBottomColor : 'green',
    },
    underline: {
      flexDirection : 'row',
      alignItems: 'center'
    },
    Indicator: {
        justifyContent: 'center', 
        flex: 1
    },
    textfield: {
        width: width*5/10, 
        fontFamily: 'Raleway-Regular', 
        color:'#666',
    },
    position1 : {
      paddingLeft : 15 ,
      fontFamily: 'Raleway-Medium',
      fontSize : 15
    },
    font : {
      fontSize : 20,
      color : 'green',
      top : 10,
      marginLeft : 10,
    },
    container2 : {  
      flexDirection: 'column',
    },
    button1 : {
      marginBottom: 20,
      flexDirection: 'row',
      alignItems: 'center',
      marginLeft: 38,
      borderBottomWidth: 0.8,
      borderBottomColor: 'gray'
    },
    margin : {
      marginLeft : 20 ,
      position : 'relative',
    },
    about : {
      left : 10 ,
      flex : 3 ,
      marginTop : '4%' 
    },
    aboutView : {
      top : 20,
      marginTop : 20 ,
      left : 5 ,
    }
});