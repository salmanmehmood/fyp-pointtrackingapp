import React, {Component} from 'react';
import {
    View,
    Modal,
    StyleSheet,
    ActivityIndicator
} from 'react-native';

export default class Spinner extends Component{
  constructor(){
    super()
  }

  render(){
    if(this.props.isOverlaySpinner) {
      return(
          <Modal
              animationType={"slide"}
              transparent={true}
              visible={this.props.isLoading}
              onRequestClose={() => {this.props.onClose && this.props.onClose()}}>
            <View style={[styles.spinner, styles.loading]}>
              <ActivityIndicator size={this.props.size || 'large'} color={this.props.color}/>
            </View>
          </Modal>
      )
    }
    return (
        <View style={styles.spinner}>
          <ActivityIndicator size={this.props.size || 'large'} color={this.props.color}/>
        </View>
    )
  }
}
const styles = StyleSheet.create({
  spinner:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  loading: {
    backgroundColor: 'rgba(0, 0, 0, .5)'
  }
});