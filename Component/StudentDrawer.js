import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView, FlatList, AsyncStorage, ImageBackground } from "react-native";
import Icon from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Expand  from 'react-native-simple-expand';
import { connect } from "react-redux";
import Middlewarefunc from './store/middleware/Middleware';

class StudentDrawerMenu extends Component {
  constructor(){
    super();
    this.state= {
        name: '',
        email: '',
        status: '',
        driverList: [],
        categoryNav: [
            {title: 'HOME', link: 'Home', isOpen: false, icon: 'home'},
            { 
                title: 'TRACK POINT', 
                isOpen: false,
                icon: 'expand-more',
                data: [
                    {
                        name: 'University--Korangi',
                        routeName: 'TraceLocation',
                        icon: 'location-pin'
                    },
                    {
                        name: 'University--SadiTown',
                        routeName: 'TraceLocation',
                        icon: 'location-pin'
                    },
                    {
                        name: 'University--Malir',
                        routeName: 'TraceLocation',
                        icon: 'location-pin'
                    }
                ]
            },
            {title: 'SETTINGS', link: 'SettingScreen', isOpen: false, icon: 'settings'},
            {title: 'LOGOUT',icon: 'log-out', isOpen: false, func: () => {
            AsyncStorage.removeItem('user')
            this.logout();
        }
        }]
    }
  }
componentWillMount(){
    this.getData();
    var driverList = [];
    let categoryNav = {...this.state.categoryNav}
    fetch(`https://rocky-stream-13314.herokuapp.com/getData`,{
        method: 'GET',
        headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
    }).then(res => res.json().then((data)=>{
        data.filter(driver => {
            if(driver.status === "driver"){
                driver.routeName = 'TraceLocation'
                driverList.push(driver)
            }
        })
        categoryNav[1].data = driverList;
        this.setState({driverList})
    })
)
}

  getData = async () => {
      const {logout} = this.props.screenProps
    try {
      const value = await AsyncStorage.getItem('user').then(val => {
        if (val !== null) {
          let parsed = JSON.parse(val);
          this.setState({
              name: parsed.firstName+" "+ parsed.lastName,
              email: parsed.email,
              status: parsed.status
          })
        }
        else{
            const {logout} = this.props.screenProps
            const {parsed} = this.props;
              this.setState({
                name: parsed.firstName+" "+ parsed.lastName,
                email: parsed.email,
                status: parsed.status
            })
        }
      })
    }
    catch (e) {}
}
  logout = () =>{
      const {logout} = this.props.screenProps;
      logout('logout')
  }

  onSelectItem(title, item ) {
      if(item.link){
          this.props.navigation.navigate(item.link)
      }
      else if(title){
          let categoryNav = this.state.categoryNav.slice();
          categoryNav.forEach((obj, i)=> {
              if(title === obj.title){
                this.setState({isOpen: !(obj.isOpen)})
              } 
            });
          if(item.func){
              item.func()
          }
      }
      let categoryNav = this.state.categoryNav.slice();
      categoryNav.forEach((obj, i)=> {obj.isOpen = (title === obj.title); });
      this.setState({categoryNav})
}

  renderMenuItem({ item }) {              
    return (
        <View> 
            <TouchableOpacity style={{ flex: 1,paddingHorizontal: 15,}} onPress={() => this.onSelectItem(item.title, item)}>
                <View style={{ paddingVertical:  12,flexDirection: 'row',alignItems: 'center'}}>
                    {item.icon === 'location-pin' || item.icon === 'log-out' ? <Icon  name={item.icon} size={25} color="green"/> : <MaterialIcons  name={item.icon} size={25} color="green"/>}
                    <Text style={{ fontSize: 15.3,color: '#2a2a2a', flex: 1, marginHorizontal: 15, fontFamily: 'Raleway-Medium'}}>{item.title}</Text>
                </View>
            </TouchableOpacity>
            <Expand value={item.isOpen} minHeight={0} maxHeight={200}>
                <View>
                    {this.renderSubMenu(item.data)}
                </View>
            </Expand>
        </View>
    );
}
selectItem = (item)=>{
    this.props.navigation.navigate(item.routeName,{point_location: item.point_location, driverId: item._id})
}

renderSubMenu(subMenus){
    if(subMenus){
        return subMenus.map((obj, index)=>{
            return(
                <TouchableOpacity key={index} onPress={()=>this.selectItem(obj)}>
                    <View style={{flexDirection: 'row', marginLeft: 50, marginBottom: 10, paddingBottom: 5}}>
                        <Icon name={obj.icon} size={18} color="green"/>
                        <Text style={{fontSize: 15, paddingLeft: 10, fontFamily: 'Raleway-Thin'}}>{obj.point_location}</Text>
                    </View>
                </TouchableOpacity>
            )
        })
    }
 };

  render(){
      const {name, email, status, driverList, categoryNav} = this.state;
    return(
      <View style = {{backgroundColor: '#f2f2f2', flex:1}}>
          <ImageBackground style={{width: '100%', height: 175, justifyContent: 'center'}} source={require('../Images/drawer-banner.png')}>
              <View style={{flexDirection: 'row', paddingLeft: 20,paddingRight: 10}}>
                  <Image style={{ width: 70, height: 70, marginRight: 15}} source={require('../Images/avatar.png')} />
                  <View style={{flex: 1}}>
                      <Text style={{color: 'white', fontSize: 18, marginTop: 12, marginBottom: 3, fontFamily: 'Raleway-Bold'}}>{name}</Text>
                      <Text style={{color: 'white', fontSize: 14, width: '100%', fontFamily: 'Raleway-Regular'}}>{email}</Text>
                      <Text style={{color: 'white', fontSize: 14, width: '100%', fontFamily: 'Raleway-Regular'}}>{status}</Text>                      
                  </View>
              </View>
          </ImageBackground>
      <ScrollView>
          <View style={{flex: 1, paddingHorizontal: 15}}>
                <FlatList
                  data={categoryNav}
                  renderItem={this.renderMenuItem.bind(this)}
                  keyExtractor={(item)=> item.title}
                  />
          </View>
      </ScrollView>
  </View>
    )
  }
}
function mapStateToProps(state){
    return{
        parsed: state.AUTHENTICATION,
        AllUserList: state.AUTHENTICATION.allUser
    }
}
function mapDispatchToProps(dispatch){
    return{
        getAllUsers: () => {
            return dispatch(Middlewarefunc.getUsers())
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(StudentDrawerMenu)