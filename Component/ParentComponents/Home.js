import React from 'react';
import { Text, View, TextInput, TouchableOpacity, Dimensions, Alert } from 'react-native';
import MapView from 'react-native-maps';
import Icon from 'react-native-vector-icons/Entypo';
import EmailIcon from 'react-native-vector-icons/FontAwesome';
import TraceStyle from './HomeStyle';
import Modal from 'react-native-modalbox';
import Spinner from '../Shared/Spinner';


export default class ParentScreen extends React.Component{
    constructor(){
        super();
        this.state={
            isDisabled: false,
            swipeToClose: true,
            email: '',
            isLoading: false,
            mapRegion: null,
            lastLong: null,
            lastLat: null,
        }
    }
    onChange = (name, val) => {
        this.setState({ [name]: val })
    } 

    findChildLocation = () => {
        this.setState({isLoading: true})
        if(!(this.state.email)){
            Alert.alert('Message', 'Field is required!')
            this.setState({isLoading: false})
        }
        else{
            fetch(`https://rocky-stream-13314.herokuapp.com/findChildLocation`,{
                method: 'POST',
                headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                body: JSON.stringify({
                    email: this.state.email.toLocaleLowerCase()
                })
            })
            .then((res)=>{
                res.json().then(data => {
                    let region = {
                        latitude : data.latitude,
                        longitude : data.longitude,
                        latitudeDelta : 0.00922*1.5,
                        longitudeDelta : 0.00421*1.5
                    }
                    this.setState({
                        mapRegion: region,
                        lastLat: region.latitude,
                        lastLong: region.longitude,
                        isLoading: false
                    })
                })
            })
            this.refs.modal3.close()
        }
    }

    render(){
        return(
            <View style={[TraceStyle.map,{flex:1}]}>
                <MapView style={TraceStyle.map}
                    region={this.state.mapRegion}
                    showsUserLocation={true}
                    followsUserLocation={true}>
                <MapView.Marker draggable
                    coordinate={{
                    latitude: (this.state.lastLat) || -36.82339,
                    longitude: (this.state.lastLong) || -73.03569,
                    }}>
                </MapView.Marker>
                </MapView> 
                    <Modal style={TraceStyle.modal3} ref={"modal3"} isDisabled={this.state.isDisabled} swipeToClose={true}>
                        <View style={{height: 45, backgroundColor: 'green', justifyContent: 'center'}}>
                            <Text style={TraceStyle.modelHeader}>STUDENT EMAIL</Text>
                        </View>
                        <View style={TraceStyle.container2}>
                            <View style={TraceStyle.underline}>
                                <EmailIcon name='user-circle' size={20} color="green"/> 
                                <Text style={TraceStyle.position1}> EMAIL :  </Text>
                            </View>
                            <View style={TraceStyle.button1}>
                                <TextInput underlineColorAndroid='transparent' style={TraceStyle.textfield} 
                                    placeholder="Enter email..." onChangeText={this.onChange.bind(this, 'email')} />
                            </View>
                            <View style={{height: 45, width: 270, backgroundColor: 'green', alignItems: 'center', justifyContent: 'center'}}>
                            <TouchableOpacity onPress={this.findChildLocation}>
                                <Text style={{color: 'white', fontSize: 20, fontFamily: 'Raleway-Regular'}}>SUBMIT</Text>
                            </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                    <TouchableOpacity style={TraceStyle.btnAlignmentEnd} onPress={() => this.refs.modal3.open()}> 
                        <View style={TraceStyle.mapButton}>
                            <View style={TraceStyle.btnView}>
                                <Icon  name='location-pin' size={35} color="white"/>
                            </View>
                        </View>
                    </TouchableOpacity>
                <Spinner color="#fff" isLoading={this.state.isLoading} isOverlaySpinner={true}></Spinner>
            </View>
        )
    }
}
