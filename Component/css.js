import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    // Front Page CSS
    frontPageContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
      },
      headerImage : {
        alignSelf : 'stretch',
        margin : 0 ,
      },
      frontPageLogo:{
        marginLeft : 10,   
      },  
      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
      instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
      },
      frontPageButton : {
        margin : 40,  
        backgroundColor : '#0b6623',
      },
      frontPageBtnText : {
        color : 'white',
        fontSize : 20,
        fontFamily: 'Raleway-Light',
      },
     frontPageFooter: {
        width : null,
        backgroundColor: '#F5FCFF',
      },
      // map css
      map  : {
        ...StyleSheet.absoluteFillObject
      },
      mapheader : {
          alignItems : 'center',
          justifyContent : 'center',
          marginTop : 15,
          fontFamily: 'Raleway-Light',
          fontSize :25
      },

    //   Login And Signup CSS 
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent : 'center',
        alignItems : 'center',
      },
      container1: {
          flex: 0,
          justifyContent : 'center',
          alignItems : 'center',
          marginTop : 80 ,
      },
      login : {
          height : 400,
          width : 300,
          justifyContent : 'center',
          alignItems : 'center',
          shadowOffset: {
              width: -6,
              height: -6,
            },
          shadowRadius : 6,
          shadowColor: '#212121',
          borderRadius : 40,
          elevation: 6,
          backgroundColor : 'white' ,
      },
      textField : {
         marginBottom : 23 ,
         paddingBottom : 3, 
         paddingLeft : 15,
         paddingRight : 15,
         borderBottomWidth : 0 ,
      },
      underline : {
          borderBottomWidth : 1,
          borderColor : 'black',
          
      },
      input : {
          marginTop : 40 ,
          
      },
      bgstyle : {
          width : '100%',
          height : '100%',
      },
      icons : {
          color : '#096b09',
          borderColor : 'white',
      },
      head : {
          fontSize : 23,
          fontFamily: 'Raleway-Light',
      },
      button : {
        borderRadius : 8,
        margin : 15,  
        backgroundColor : '#0b6623',
      },
      btnText : {
          color : 'white',
          fontSize : 20,
          fontFamily: 'Raleway-Light',
    },
    Signuphead : {
        marginTop:12,
        fontSize : 23,
        fontFamily: 'Raleway-Bold',
        justifyContent : 'flex-start',
    },
    Signupradio : {
        flex : 1,
        flexDirection : 'row',
        justifyContent : 'flex-start',
    },
    radioText : {
        fontSize : 20, 
    },
    radioList : {
        flexDirection : 'row',
        alignItems : 'flex-start',
    },
    radioborder :{
        borderBottomWidth : 0,
        justifyContent : 'flex-start',
    },
    Signugender : {
        flexDirection : 'row',
        marginLeft : 20,
    },
    iconsgender:{
        color : '#096b09',
        borderColor : 'white',
        fontSize : 32,
    },
    gendertxt : {
        fontSize : 19,
        fontFamily: 'Raleway-Light',
        justifyContent : 'flex-start',
    },
    List : {
        flex:1,
        flexDirection:'column',
        alignItems:'flex-start',
        marginLeft: -82,
        marginTop:20,
    },
    nxtpage : {
        flexDirection : 'row',
        justifyContent : "flex-start",
        margin : 30,
    },
    btn1:{
        fontSize : 15,
        marginRight : 30,
        paddingTop : 1,
        width : 25,
        height : 25,
        textAlign : 'center',
        color : 'white', 
        backgroundColor : '#096b09',
        borderRadius : 20,
        alignItems : 'center',
       
    },
    btn2 :{
        fontSize : 15,
        width : 25 ,
        color : "#096b09",
        elevation:8,
    },
    username : {
        marginBottom : 15 ,
        paddingBottom : 3, 
        paddingLeft : 15,
        paddingRight : 15,
        borderBottomWidth : 0 ,
    },
    input : {
        marginTop : 10,
    },
    iconsenvelope:{
        color : '#096b09',
        borderColor : 'white',
        fontSize : 27,
    },    iconslock:{
        marginLeft:3,
        color : '#096b09',
        borderColor : 'white',
        fontSize : 35,
    },    
    iconscheck:{
        marginLeft:-2,
        color : '#096b09',
        borderColor : 'white',
        fontSize : 32,
    },
    submitbutton :  {
        position : 'relative',
        top : 15,
    },    
    signbtn : {
        borderColor : 'black',
        borderRadius : 15 ,
        backgroundColor : 'green',
        height : 50 ,
        width :180 ,
        justifyContent : 'center',
        alignItems : 'center',
    },
    btn3 : {
        marginRight : 30,
        color : '#096b09', 
        fontSize : 15,
    },
    btn4 : {
        fontSize : 15,
        marginRight : 30,
        paddingTop : 1,
        width : 25,
        height : 25,
        textAlign : 'center',
        color : 'white', 
        backgroundColor : '#096b09',
        borderRadius : 20,
        alignItems : 'center',
    },
    footer : {
        alignItems : 'center',
        marginRight : 5,
    },
    // Drawer CSS start
    menuIcon : {
        alignItems : 'flex-start',
        justifyContent : 'flex-start' ,
        backgroundColor :"green"
    },
    settingIcon : {
        color : 'green',
        // paddingTop : 10,
    },
    DrawerHeader : {
        height : 200 , 
        backgroundColor : '#2d2d2d'
    },
    DrawerBody : {
        alignItems : 'center',
        justifyContent : "center"
    },
    drawerItem : {
        marginTop : 50,
    },
    DrawerLogo : {
        height:150,
        width:180
    },
    // Admin Panel
    adminsearch : {
        borderColor : 'black',
        borderWidth : 2,
        marginTop : 5
    },
    addbuttonview : {
        justifyContent : 'center'
    },
    addbutton : {
        fontSize : 35,
        backgroundColor : '#096b09',
        width : 52,
        height : 52,
        borderRadius : 25,
        textAlign : 'center',
        color : 'white',
        
    },
    addbuttonopacity : {
        justifyContent : 'center',
        alignItems : 'center'
    },
    adminFooter : {
        backgroundColor : 'white',
        bottom : 0,
    },
    AddDriver : {
        height : 500,
        width : 300,
        justifyContent : 'center',
        alignItems : 'center',
        shadowOffset: {
            width: -6,
            height: -6,
          },
        shadowRadius : 6,
        shadowColor: '#212121',
        borderRadius : 40,
        elevation: 6,
        backgroundColor : 'white' ,
    },
    setDriverScreen : {
        flex : 1,
        justifyContent : 'center',
        alignItems : 'center',
        // margin : 0
    }
});
