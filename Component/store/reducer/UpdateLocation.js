import ActionTypes from '../action/actiontype';

const initial_State = {

}

function UPDATELOCATION( state = initial_State , action )
{
    switch(action.type){
        
        case ActionTypes.lOCATION : {
            return Object.assign( {}, state, action.val )
        }
        default : {
            return state   
        } 
    }
}
export default UPDATELOCATION