
import {createStore , combineReducers , applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import AUTHENTICATION from './reducer/Authentication';
import CV_DATA from './reducer/Getdata';
import UPDATELOCATION from './reducer/UpdateLocation'

const middleware = applyMiddleware(thunk)
export const rootreducers = combineReducers({
    AUTHENTICATION,
    CV_DATA,
    UPDATELOCATION
})

let store = createStore(rootreducers, middleware);

store.subscribe(()=>{
})

export default store;