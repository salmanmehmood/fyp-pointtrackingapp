import React from 'react';
import {Platform, StyleSheet, Text, View, AsyncStorage} from 'react-native';
import {Container ,Content, Header, Body, Icon, Left} from 'native-base';
import HomeStyle from './HomeStyle';
import MapView from 'react-native-maps';

export default class HomeScreen extends React.Component{
    constructor(){
        super();
        this.state = {
            mapRegion : null,
            lastLat : null,
            lastLong : null,
            region : ''
        };
        let watchID = null;
    }
    
    componentDidMount(){
        this.getCurrentLocation()
    }

    getCurrentLocation(){
        let { _id } = this.props.screenProps;
        this.watchID = navigator.geolocation.getCurrentPosition((position)=>{
            fetch(`https://rocky-stream-13314.herokuapp.com/updateLocation/${_id}`,{
                method: 'POST',
                headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                body: JSON.stringify({ 
                    longitude: position.coords.longitude,
                    latitude: position.coords.latitude,
                })
            })
            let region = {
                latitude : position.coords.latitude,
                longitude : position.coords.longitude,
                latitudeDelta : 0.00922*1.5,
                longitudeDelta : 0.00421*1.5
            }
            this.setState({
                mapRegion : region,
                lastLat : region.latitude,
                lastLong : region.longitude
            }),
            { enableHighAccuracy: true, timeout: 200000, maximumAge: 1000 }
        })
    }

    componentWillUnmount(){
        navigator.geolocation.clearWatch(this.watchID)
    }
    static navigationOptions = {
        drawerIcon : (
            <Icon name ="home" style={HomeStyle.settingIcon}/>
        )
    }
    
    render(){
        return(
            <Container>
                <Content contentContainerStyle={{flex :1, alignItems : 'center',justifyContent : "center"}}>
                    <MapView style={HomeStyle.map}
                        region={this.state.mapRegion}
                        showsUserLocation={true}
                        followsUserLocation={true}>
                    <MapView.Marker draggable
                        coordinate={{
                        latitude: (this.state.lastLat) || -36.82339,
                        longitude: (this.state.lastLong) || -73.03569,
                        }}>
                    </MapView.Marker>
                    </MapView>
                </Content>
            </Container>
        )
    }
}
