import { StyleSheet } from 'react-native';

export default HomeStyle = StyleSheet.create({
      map  : {
        ...StyleSheet.absoluteFillObject
      },

    settingIcon : {
        color : 'green',
    },
});
