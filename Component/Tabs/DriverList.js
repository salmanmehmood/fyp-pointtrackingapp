import React from 'react';
import {ToastAndroid, Platform, StyleSheet, Text, FlatList, View, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native';
import { Container, Input, Item, Footer } from 'native-base';
import styles from '../css'; 
import Icon from 'react-native-vector-icons/FontAwesome';
import IconCross from 'react-native-vector-icons/Entypo';
import Middlewarefunc from '../store/middleware/Middleware';

import { connect } from 'react-redux';    

class DriverList extends React.Component {
    constructor(){
        super();
        this.state={
            driverList: [],
            copyDriverList: []
        }
    }
    componentDidMount(){
        this.props.getAllUsers();
    }

    componentWillReceiveProps(nextProps){
        let users = nextProps.allUsers;
        driverList = [];
        for(var key in users){
            if(users[key].status === 'driver'){
                users[key].name = users[key].firstName + " " +users[key].lastName 
                driverList.push(users[key])
            }
            this.setState({
                driverList,
                copyDriverList: driverList
            })
        }
    }

    deleteUser = (index) =>{
        let { driverList } = this.state;
        fetch(`https://rocky-stream-13314.herokuapp.com/deleteUser/${driverList[index]._id}`,{
            method : 'DELETE'
        })
        ToastAndroid.showWithGravityAndOffset(`${driverList[index].firstName} Remove From Driver List`, ToastAndroid.LONG, ToastAndroid.BOTTOM, 25, 50);
        driverList.splice(index,1)
        this.setState({driverList})
    }

    renderDriverList({ item, index }){
        return( 
            <View style={{marginHorizontal: 5, marginVertical: 8,}}>
                <View style={{marginHorizontal: 2, borderRadius:10, backgroundColor: '#fff',padding: 15,shadowColor: '#000', shadowOpacity: 0.55, shadowRadius: 2.98, elevation: 2, flexDirection: 'column'}}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{width: '93%', fontSize: 15, fontFamily: 'Raleway-Light'}}><Text style={{fontFamily: 'Raleway-Regular'}}>NAME</Text>: { item.firstName.toUpperCase() +" "+ item.lastName.toUpperCase() }</Text>
                        <IconCross size={23} onPress={()=>this.deleteUser(index)} name='cross' color='red'/>                            
                    </View>
                    <Text style={{fontFamily: 'Raleway-Regular',paddingVertical: 4}}>Email<Text style={{fontFamily: 'Raleway-Light'}}>: { item.email }</Text></Text>
                    <Text style={{fontFamily: 'Raleway-Regular',paddingVertical: 4}}>DESTINATION<Text style={{fontFamily: 'Raleway-Light'}}>: { item.point_location.toUpperCase() }</Text></Text>
                    <Text style={{fontFamily: 'Raleway-Regular',paddingVertical: 4}}>STATUS<Text style={{fontFamily: 'Raleway-Light'}}>: { item.status.toUpperCase() }</Text></Text>                        
                </View>  
            </View>
        )
    }
    
    searchFilter(text){
        let search = text.toLowerCase();
        this.setState({
            copyDriverList: this.state.driverList.filter(obj => obj.name.toLowerCase().includes(search))
        });
    }

    render() {
        let { driverList, copyDriverList } = this.state;
      return (
        <Container>
            <View style={styles.adminsearch}>
                <Item style={styles.textField}>
                        <Input placeholder = 'Search By Name' style={styles.underline} onChangeText={(val)=>this.searchFilter(val)} placeholderTextColor="#C0C0C0"/>
                        <Icon name='search' size={20} color='green' />
                </Item>
            </View>
            <ScrollView>
                {
                copyDriverList.length !== 0 ? 
                <FlatList
                    data={copyDriverList}
                    renderItem={this.renderDriverList.bind(this)}
                    keyExtractor={ ( item, index ) => item.email}
                />
                :<View style={{justifyContent: 'center', flex: 1}}><ActivityIndicator size={30}/></View> 
                }
            </ScrollView>
        </Container>
      );
    }
  }
function mapStateToProps(state){
      return{
          allUsers: state.AUTHENTICATION.allUser   
      }
  }
function mapDispatchToProps(dispatch){
    return{
        getAllUsers: () =>{
            return dispatch(Middlewarefunc.getUsers())
        }
    }
  }
export default connect(mapStateToProps, mapDispatchToProps)(DriverList)