import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';
import Frontpage from './Component/FrontPage';
import { NativeRouter, Route } from 'react-router-native';
import Login from './Component/Login/Login';
import Signup from './Component/SignUp/Signup';
import Drawer from './Component/Drawer';
import AddDriver from './Component/AddDriver';
import store from './Component/store';
import { Provider } from 'react-redux';
import { Auth, DriverStackDrawer, StudentStackDrawer, AdminStackDrawer, ParentStackDrawer } from './Component/Routes/Router';
import DriverDrawer from './Component/DriverDrawer';


export default class App extends Component {
 
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      isLogin: '',
      _id: ''
    }
  }

  toggleScreen = (value, _id)=> {
    this.setState({ isLogin: value, _id })
  }
  
  renderUi = () =>{
    let {loading, isLogin, _id} = this.state;
    if (loading) {
      return (
        <View>
          <Text>Loading...</Text>
        </View>
      ) 
    }
    else if(isLogin === 'driver'){
      return(<DriverStackDrawer screenProps={{ logout: () => this.toggleScreen(), _id }} /> )
    }
    else if(isLogin === 'student'){
      return(<StudentStackDrawer screenProps={{ logout: () => this.toggleScreen(), _id }} /> )
    }
    else if(isLogin === 'parent'){
      return <ParentStackDrawer screenProps={{ logout: () => this.toggleScreen(), _id }}/>
    }
    else if(isLogin === 'admin'){
      return <AdminStackDrawer screenProps={{ logout: () => this.toggleScreen(), _id }} />      
    }
    else{
      return(<Auth screenProps={{ login: this.toggleScreen.bind(this)}} />)
    }
  }

  render() {
    return (
      <Provider store={store}>
        {this.renderUi()}
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  }
});
